#ifndef __PWM_SVS_H
#define __PWM_SVS_H


#include <stdbool.h>
#include <stdint.h>


typedef struct
{
    uint16_t    Frequency;
    uint16_t    Prescaler;
    
    uint16_t    Pulse;
    
    bool        InUse;
    
    bool        InChange;
}
PWM_SVS_Channel;

typedef struct
{
    PWM_SVS_Channel ChannelOne;
    
    PWM_SVS_Channel ChannelTwo;
    
    PWM_SVS_Channel ChannelThree;
    
    uint16_t        DigitOne; // First (MS) digit of led displays.
    
    uint16_t        DigitTwo; // Second digit of led displays.
    
    uint16_t        DigitThree; // Last (LS) digit of led displays.
    
    uint8_t         DigitNumber; // Number of current visible digit. 0 - none, 1 .. 3 is digits 1 .. 3.
    
    bool            RefillDigitsFlag; // Need refill digits of led displays.
}
PWM_SVS_HandleTypeDef;


void PWM_SVS_Start(PWM_SVS_HandleTypeDef *hPwm);


#endif /* __PWM_SVS_H */
