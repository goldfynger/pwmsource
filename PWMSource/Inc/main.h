/**
  ******************************************************************************
  * File Name          : main.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define ENC_A_Pin GPIO_PIN_0
#define ENC_A_GPIO_Port GPIOA
#define ENC_B_Pin GPIO_PIN_1
#define ENC_B_GPIO_Port GPIOA
#define ENC_P_Pin GPIO_PIN_2
#define ENC_P_GPIO_Port GPIOA
#define DIG1_CTRL_Pin GPIO_PIN_3
#define DIG1_CTRL_GPIO_Port GPIOA
#define PWM1_CTRL_Pin GPIO_PIN_4
#define PWM1_CTRL_GPIO_Port GPIOA
#define DIG2_CTRL_Pin GPIO_PIN_5
#define DIG2_CTRL_GPIO_Port GPIOA
#define PWM2_CTRL_Pin GPIO_PIN_6
#define PWM2_CTRL_GPIO_Port GPIOA
#define PWM3_CTRL_Pin GPIO_PIN_7
#define PWM3_CTRL_GPIO_Port GPIOA
#define DIG3_CTRL_Pin GPIO_PIN_0
#define DIG3_CTRL_GPIO_Port GPIOB
#define REG_DS_Pin GPIO_PIN_1
#define REG_DS_GPIO_Port GPIOB
#define REG_ST_Pin GPIO_PIN_8
#define REG_ST_GPIO_Port GPIOA
#define B_TX_Pin GPIO_PIN_9
#define B_TX_GPIO_Port GPIOA
#define B_RX_Pin GPIO_PIN_10
#define B_RX_GPIO_Port GPIOA
#define REG_LT_Pin GPIO_PIN_11
#define REG_LT_GPIO_Port GPIOA
#define SWDIO_Pin GPIO_PIN_13
#define SWDIO_GPIO_Port GPIOA
#define SWCLK_Pin GPIO_PIN_14
#define SWCLK_GPIO_Port GPIOA
#define PWM1_EN_Pin GPIO_PIN_15
#define PWM1_EN_GPIO_Port GPIOA
#define PWM1_SEL_Pin GPIO_PIN_3
#define PWM1_SEL_GPIO_Port GPIOB
#define PWM2_EN_Pin GPIO_PIN_4
#define PWM2_EN_GPIO_Port GPIOB
#define PWM2_SEL_Pin GPIO_PIN_5
#define PWM2_SEL_GPIO_Port GPIOB
#define PWM3_EN_Pin GPIO_PIN_6
#define PWM3_EN_GPIO_Port GPIOB
#define PWM3_SEL_Pin GPIO_PIN_7
#define PWM3_SEL_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
