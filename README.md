## PWM Source

Firmware for 3-channel PWM source with independent selectable voltages, frequencies and pulse widths.

[Hardware](https://easyeda.com/goldfynger/PWMSource-70e2b1eb6d8047f7aecead3ec34539d2)